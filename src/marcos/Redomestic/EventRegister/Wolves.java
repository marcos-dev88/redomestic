package marcos.Redomestic.EventRegister;

import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.inventory.ItemStack;

public class Wolves {

	private static Wolves instance;

	public static Wolves getInstance() {
		if (instance == null) {
			instance = new Wolves();
		}
		return instance;
	}

	public ItemStack steakBeef() {

		ItemStack beef = new ItemStack(Material.RAW_BEEF);

		return beef;
	}

	@SuppressWarnings("deprecation")
	public void Interact(Player player, Entity wolf) {
		if (wolf instanceof Wolf) {
			Wolf wolfEnty = (Wolf) wolf;
			if (wolfEnty.isSitting()) {
				if (player.getItemInHand().isSimilar(steakBeef())) {
					if (wolfEnty.getType().equals(EntityType.WOLF)) {
						if (!wolfEnty.getOwner().getUniqueId().equals(player.getUniqueId())) {
							int rndon = new Random().nextInt(15);
							if (rndon >= 13) {
								player.sendMessage(ChatColor.GOLD + "It is your now!");
								wolfEnty.setOwner(player);
								wolfEnty.setAngry(false);
							}
							if (wolfEnty.isTamed()) {
								player.getInventory().getItemInHand().setAmount(player.getItemInHand().getAmount() - 1);
							}
						}
					}
				}
			}
		}
	}
}
