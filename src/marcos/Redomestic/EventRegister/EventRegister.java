package marcos.Redomestic.EventRegister;

import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;

public class EventRegister implements Listener {

	private Wolves wolf = Wolves.getInstance();

	@EventHandler
	public void PlayerInteract(PlayerInteractEntityEvent event) {

		Player player = event.getPlayer();
		if (event.getRightClicked() instanceof Wolf) {
			Wolf wolfInteragido = (Wolf) event.getRightClicked();
			wolf.Interact(player, (Wolf) wolfInteragido);
		}
	}
}
