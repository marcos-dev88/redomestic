package marcos.Redomestic.Wolves;


import org.bukkit.ChatColor;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import marcos.Redomestic.EventRegister.EventRegister;

public class Main extends JavaPlugin {
	
	PluginManager pm = (PluginManager) this.getServer().getPluginManager();
	
	@Override
	public void onLoad() {
		this.getServer().broadcastMessage(ChatColor.AQUA+"============================");
		this.getServer().broadcastMessage(ChatColor.GREEN+" Plugin of redomestic wolves");
		this.getServer().broadcastMessage(ChatColor.AQUA+"============================");
		
		super.onLoad();
	}
	
	@Override
	public void onEnable() {
		
	pm.registerEvents(new EventRegister(), this);
		
		super.onEnable();
	}
	
	@Override
	public void onDisable() {
		
		HandlerList.unregisterAll();
		super.onDisable();
		
	}
	
	
	
}
